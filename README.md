Suckless DWM with patches.

One branch for each patch:

1 Fake fullscreen

2 PerTag

3 AttachBottom

4 Center

5 NoBorder

6 ... and a Config Branch

I use an ErgoDox keyboard so I have more "meta" style keys than you probably do. If you are going to use this version of DWM, then please do customise the launchers first. Other wise I worry that you will have a bad time.
